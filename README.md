# Nevalyashka Cloud - Cloud Service Provider

============

Nevalyashka Cloud is a Cloud Service Provider written in Spring and based on AWS API.

---

## Features
- Load Balancer
- Auto CI/CD

---


## Setup
Clone this repo:

```
git clone git@gitlab.com:andreiliphd/nevalyashka-cloud.git
```
---


## Usage
-  Run
---

## Changelog
- Set up Load Balancer
- CI/CD pipeline
---

## License
MIT