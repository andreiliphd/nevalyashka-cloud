package com.andreiliphd.nevalyashka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NevalyashkaCloud {
	public static void main(String[] args) {
		SpringApplication.run(NevalyashkaCloud.class, args);
	}
}