package com.andreiliphd.nevalyashka.controller;

import com.andreiliphd.nevalyashka.models.Client;
import com.andreiliphd.nevalyashka.models.KeyPair;
import com.andreiliphd.nevalyashka.models.FormInstance;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

@Controller
public class MainController {
    private Region region = Region.US_WEST_2;
    private Ec2Client ec2 = Ec2Client.builder()
                .region(region)
                .build();

    @GetMapping("/")
    public String getMain(Model model) {
    return "main";
    }

    @GetMapping("/createInstance")
    public ResponseEntity<Object> createInstance(@RequestParam(name="name", defaultValue = "andrei") String name,
                                 @RequestParam(name="ami", defaultValue = "ami-0174313b5af8423d7") String ami,
                                 Model model) {
        String instanceId = null;
        ArrayList<Object> data = Client.describeEC2Instances(this.ec2) ;
        if (data.size() >= 6) {
            System.out.println("Max instances attained");
        } else {
            instanceId = Client.createEC2Instance(this.ec2, name, ami);
        }

        model.addAttribute("instance_id", instanceId);
        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(model, httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/describeInstances")
    public ResponseEntity<Object> describeInstances(@RequestParam(name="id", required=false) String id, Model model) {
        ArrayList<Object> data = Client.describeEC2Instances(this.ec2) ;
        model.addAttribute("instances", data);
        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(data, httpHeaders, HttpStatus.OK);
    }


    @GetMapping("/stopInstance")
    public String stopInstance(Model model) {
        model.addAttribute("instance", new FormInstance());
        return "stop";
    }


    @PostMapping("/stopInstance")
    public ResponseEntity<Object> stopInstanceSubmit(@ModelAttribute FormInstance instance) {
        HashMap model = new LinkedHashMap<>();
        try {
            Client.stopInstance(this.ec2, instance.getId()) ;
            model.put("result", true);
            model.put("instance_id", instance.getId());
        } catch (Exception e) {
            model.put("result", false);
            model.put("instance_id", instance.getId());
            model.put("reason", "Instance not found");
        }
        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(model, httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/createKeyPair")
    public ResponseEntity<Object> createKeyPair(@RequestParam(name="key") String key, Model model){
        Boolean created = KeyPair.createEC2KeyPair(this.ec2, key);
        model.addAttribute(key, created);
        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(created, httpHeaders, HttpStatus.OK);
    }
}